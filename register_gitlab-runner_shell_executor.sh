NETWORK_SETTINGS_LOCATION=https://gitlab.com/shell-bootstrap-scripts/network-settings/-/raw/master/set_variables_in_CI.sh
FIX_ALL_GOTCHAS_SCRIPT_LOCATION=https://gitlab.com/shell-bootstrap-scripts/shell-bootstrap-scripts/-/raw/master/fix_all_gotchas.sh
CI_SERVER_URL=https://gitlab.com

# Check that timeout is available before we try to register.
if ! timeout --help; then exit 1; fi
if ! wget --help; then exit 1; fi
if ! cat /etc/os-release; then exit 1; fi
if ! dpkg --print-architecture; then exit 1; fi
if [ -z ${REGISTER_LOCKED+ABC} ]; then
 # When downloading this script and running it, we almost always want --locked=false.
 # For testing purposes, we can set LOCKED=true in the GitLab CI settings.
 REGISTER_LOCKED=false
 echo "By default, this gitlab-runner is not permanently tied to any particular repository. To change that, set REGISTER_LOCKED=true."
else
 echo "REGISTER_LOCKED=$REGISTER_LOCKED"
fi
if [ -z ${RUNNER_EXECUTOR+ABC} ]; then
 RUNNER_EXECUTOR=docker
 echo "By default, this script registers the gitlab-runner with the Docker executor. To change that, set RUNNER_EXECUTOR=shell."
else
 echo "RUNNER_EXECUTOR=$RUNNER_EXECUTOR"
fi
if [ -z ${CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN+ABC} ]; then echo "This script cannot run if CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN is undefined."; exit 1; fi
if [ -z ${CI_SERVER_URL+ABC} ]; then echo "This script cannot run if CI_SERVER_URL is undefined."; exit 1; fi

if [ -z ${NETWORK_SETTINGS_LOCATION+ABC} ] || [ -z ${FIX_ALL_GOTCHAS_SCRIPT_LOCATION+ABC} ]; then
 echo "This script is intended to be run with NETWORK_SETTINGS_LOCATION and FIX_ALL_GOTCHAS_SCRIPT_LOCATION set. However, we will gamely attempt to proceed without the setup scripts."
else
 # We cannot check the certificate before we have the certificate.
 wget --no-check-certificate $NETWORK_SETTINGS_LOCATION --output-document environment.sh
 cat environment.sh
 wget --no-check-certificate $FIX_ALL_GOTCHAS_SCRIPT_LOCATION
 . ./fix_all_gotchas.sh
fi
apt-get update
# # With the shell executor, the host must have git else you can gitlab-runner run but all jobs will fail with git: command not found
apt-get install --assume-yes git
GITLAB_RUNNER_NIGHTLY_BUILD_URL=https://s3.amazonaws.com/gitlab-runner-downloads/master/binaries/gitlab-runner-linux-$(dpkg --print-architecture)
echo $GITLAB_RUNNER_NIGHTLY_BUILD_URL
wget $GITLAB_RUNNER_NIGHTLY_BUILD_URL --output-document=gitlab-runner
chmod u+x gitlab-runner

# set -o allexport
. /etc/os-release
# set +o allexport
echo $ID
echo $VERSION_ID
echo $VERSION_CODENAME
if [ -z ${ID+ABC} ]; then echo "ID is undefined, something is wrong."; exit 1; fi
if [ -z ${VERSION_ID+ABC} ]; then echo "VERSION_ID is undefined, something is wrong."; exit 1; fi
if [ -z ${VERSION_CODENAME+ABC} ]; then echo "VERSION_CODENAME is undefined, something is wrong."; exit 1; fi
RUNNER_TAG_LIST="$RUNNER_EXECUTOR,$ID,$ID$VERSION_ID,$VERSION_CODENAME,relatime"
echo "RUNNER_TAG_LIST=$RUNNER_TAG_LIST"

./gitlab-runner register --help
echo $REGISTER_LOCKED
echo $REGISTER_ACCESS_LEVEL
# Do we want to use --tls-ca-file or --tls-cert-file?
# later might want to use --env and/or --pre-build-script
# Variables set outside this script, such as RUNNER_EXECUTOR, are carried in,
# but RUNNER_TAG_LIST set inside this script needs to be explicitly given to gitlab-runner.
RUNNER_TAG_LIST=$RUNNER_TAG_LIST ./gitlab-runner register --non-interactive --registration-token $CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN --url $CI_SERVER_URL

# In case a runner fails to get properly unregistered, we want to display something like
# token = "__REDACTED__"
# since we don't know who might see these logs.
# Right now this just deletes the whole token line because that's easier to do.
cat /etc/gitlab-runner/config.toml | sed '/token = /d'

